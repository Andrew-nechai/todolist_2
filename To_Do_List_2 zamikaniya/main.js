let id_element1 = document.getElementById("wrapper1");
let id_element2 = document.getElementById("wrapper2");

function CopyBlock(id_element){


	//Создание внешнего вида todolist, стилизация
	this.$wrapper = id_element.querySelector('.background_of_todolist');
	this.$wrapper.classList.add("background_of_todolist");

	this.$inputLine = id_element.querySelector(".input-style");

	this.$button_add = id_element.querySelector(".input-button");

	this.$image_plus = id_element.querySelector(".input-button-img");

	this.image_checkbox_src_check = "img\\checked.svg";
	this.image_checkbox_src_uncheck = "img\\unchecked.svg";

	this.image_delete_deal_src = "img\\delete.svg";
	
	this.$content_block = id_element.querySelector(".content");

	this.$count_block = id_element.querySelector(".count");
	this.$count_block.innerText = "Unchecked tasks: ";

	this._deals_mass = [];

	this.$tool_block = id_element.querySelector(".tools");
	this.$tool_block_button_all = id_element.querySelector(".all");
	this.$tool_block_button_all.classList.add("button-off");
	this.$tool_block_button_active = id_element.querySelector(".active");
	this.$tool_block_button_active.classList.add("button-off");
	this.$tool_block_button_complete = id_element.querySelector(".complete");
	this.$tool_block_button_complete.classList.add("button-off");
	this.$tool_block_button_clear =id_element.querySelector(".clear-items");

	
	//добавление дела через Enter
	this.btnClickAdd = function a(key) {
		if (key.keyCode == 13) this.addDeal();
	}
	
	this.$inputLine.onkeyup = this.btnClickAdd.bind(this);
	
	this.createDeal = function(item_add) {

		//создание дела
		let deal 			   = document.createElement("div");
		let button_checkbox    = document.createElement("button");
		let image_checkbox     = document.createElement('img');
		let input_content      = document.createElement("div");
		let button_delete_deal = document.createElement("button");
		let image_delete_deal  = document.createElement('img');


		//стилизация и объединение
		deal.classList.add("deal");

		if (item_add.check == "checked") {
			console.log("yes");
			image_checkbox.src = this.image_checkbox_src_check;
			button_checkbox.classList.add("checked");
		} else {
			image_checkbox.src = this.image_checkbox_src_uncheck;
			button_checkbox.classList.add("unchecked");
		}
		button_checkbox.appendChild(image_checkbox);

		input_content.innerText = item_add.task;

		button_delete_deal.classList.add("trush");
		image_delete_deal.src = this.image_delete_deal_src;
		button_delete_deal.appendChild(image_delete_deal);


		//методы
		button_delete_deal.onclick = function(){ 
			let trush = this.parentNode;
			this.parentNode.parentNode.removeChild(trush);
			item_add.existance = false;
		}

		button_delete_deal.addEventListener('click', this.removeShadow.bind(this));
		button_delete_deal.addEventListener('click', this.countUncheckDeals.bind(this));
		button_delete_deal.addEventListener('click', this.deleteDeals.bind(this))
	
		deal.onclick = function(){
			deal.classList.toggle("red-boxshadow");
		}

		deal.addEventListener('click', this.resetToolbuttonsStyle.bind(this));


		button_checkbox.addEventListener('click', function () {
			if (this.className == "unchecked"){
				this.className = "checked";
				this.firstChild.src = "img\\checked.svg";
				item_add.check = "checked";
			} else {
				this.className = "unchecked";
				this.firstChild.src = "img\\unchecked.svg"
				item_add.check = "unchecked";
			}	
		});

		button_checkbox.addEventListener('click', this.button_checkbox_onclick.bind(this));
		

		// создание копии
		deal.appendChild(button_checkbox);
		deal.appendChild(input_content);
		deal.appendChild(button_delete_deal);
		this.$content_block.insertBefore(deal, this.$content_block.firstChild);
	}

	//Ф-я создания дела и добавление его в список
	this.addDeal = function () {
		if (this.$inputLine.value == ""){
			return
		}
		console.log(id_element)
		let item_add = {
			task: this.$inputLine.value, 
			check: "unchecked", 
			existance: true
		}

		this.removeShadow();
		this.resetToolbuttonsStyle();
		this.createDeal(item_add);
		this.countUncheckDeals();


		// добавление дела в массив и в localStorage
		this._deals_mass.push(item_add);
		localStorage.setItem(id_element.id + "", JSON.stringify(this._deals_mass))


		//очистка input
		this.$inputLine.value = "";
		console.log(item_add)
	}

	//добавление дела через кнопку
	this.$button_add.addEventListener('click', this.addDeal.bind(this));


	//нажатие на checkbox
	this.button_checkbox_onclick = function () {
		this.removeShadow();
		this.resetToolbuttonsStyle();
		this.countUncheckDeals();
		event.stopPropagation();

		localStorage.setItem(id_element.id + "", JSON.stringify(this._deals_mass))
		console.log(this._deals_mass);
	}


	//удаление дел из массива
	this.deleteDeals = function () {
		let i = 0;
		while(this._deals_mass[i]){
			if (this._deals_mass[i].existance == false){
				this._deals_mass.splice(i,1);
				i--;
			}
			i++;
		}
		localStorage.setItem(id_element.id + "", JSON.stringify(this._deals_mass));
	}


	//подсчет невыполненных дел
	this.countUncheckDeals = function () {
		let i = 0, j = 0;
		while (this.$content_block.childNodes[i]){
			if (this.$content_block.childNodes[i].firstChild.classList.contains("unchecked")){
				j++;
			}
			i++;
		}
		this.$count_block.innerText = "Unchecked tasks: " + j;
	}


	//удаление красной тени
	this.removeShadow = function () {
		let i = 0;
		while (this.$content_block.childNodes[i]){
			if (this.$content_block.childNodes[i].classList.contains("red-boxshadow")){
				this.$content_block.childNodes[i].classList.toggle("red-boxshadow");
			}
			i++;
		}
	}


	//проверка content 
	this.check_empty_block = function () {
		if (this.$content_block.firstChild == null) return 0;
	}


	//проверка на наличие активных дел в блоке
	this.checkActiveDeals = function () {
		let i = 0, answer = false;
		while (this.$content_block.childNodes[i]) {
			if (this.$content_block.childNodes[i].firstChild.classList.contains("unchecked")){
				answer = true;
				break;
			}
			i++;
		}
		return answer;
	}


	//проверка на наличие выполненных дел
	this.checkCompleteDeals = function () {
		let i = 0, answer = false;
		while (this.$content_block.childNodes[i]) {
			if (this.$content_block.childNodes[i].firstChild.classList.contains("checked")){
				answer = true;
				break;
			}
			i++;
		}
		return answer;
	}


	//есть ли в content block выбранные элементы для удаления
	this.checkRedShadowElements = function () {
		let i = 0, answer = false;
		while (this.$content_block.childNodes[i]) {
			if (this.$content_block.childNodes[i].classList.contains("red-boxshadow")){
				answer = true;
				break;
			}
			i++;
		}
		return answer;
	}


	//Выделить все дела для последующего удаления
	this.selectAllDeals = function () {
		if (this.check_empty_block() == 0){
			alert("Список дел пуст!");
			return 0;
		}
		this.check_empty_block();
		this.removeShadow();
		this.changeStyleOnButtonAll();
		let i = 0;
		if (this.whatToolButtonIsClicked() == "All"){
			while (this.$content_block.childNodes[i]){
				this.$content_block.childNodes[i].classList.add("red-boxshadow");
				i++;
			}
			
		} else {
			this.removeShadow();
		}
	}


	//Выделить активные дела для последующего удаления
	this.selectActiveDeals = function () {
		if (this.checkActiveDeals() == false || this.check_empty_block() == 0){
			alert("В вашем списке нету активных дел!");
			return 0;
		}
		this.check_empty_block();
		this.removeShadow();
		this.changeStyleOnButtonActive();
		let i = 0;
		if (this.whatToolButtonIsClicked() == "Active"){
			while (this.$content_block.childNodes[i]){
				if (this.$content_block.childNodes[i].firstChild.className == "unchecked"){
					this.$content_block.childNodes[i].classList.add("red-boxshadow");
				}
				i++;
			}
		} else {
			this.removeShadow();
		}
	}


	//Выделить выполненные дела для последующего удаления
	this.selectCompleteDeals = function () {
	    if (this.checkCompleteDeals() == false || this.check_empty_block() == 0){
	    	alert("В вашем списке нету выполненных дел!");
	    	return 0;
	    }
		this.check_empty_block();
		this.removeShadow();
		this.changeStyleOnButtonComplete();
		let i = 0;
		if (this.whatToolButtonIsClicked() == "Complete"){
			while (this.$content_block.childNodes[i]){
				if (this.$content_block.childNodes[i].firstChild.className == "checked"){
					this.$content_block.childNodes[i].classList.add("red-boxshadow");
				}
				i++;
			}
		} else {
			this.removeShadow();
		}
	}


	//Метод для alert нужных фраз перед удалением дел
	this.whatToolButtonIsClicked = function () {
		if (this.$tool_block_button_all.classList.contains("button-on")) return ("All");
		if (this.$tool_block_button_active.classList.contains("button-on")) return ("Active");
		if (this.$tool_block_button_complete.classList.contains("button-on")) return ("Complete");
		else return("None");
	}


	//Нажатие на кнопку удаления дел
	this.clickOnButtonClearItems = function () {
		if (this.check_empty_block == 0){
			alert("Список дел пуст");
			return 0;
		}
		let j = this.whatToolButtonIsClicked();
		let i = 0;
		if (this.checkRedShadowElements()){
			if(confirm("Удалить выбранные дела?")){
				while(this.$content_block.childNodes[i]){
					if (this.$content_block.childNodes[i].className == "deal red-boxshadow"){ 
						this.$content_block.childNodes[i].childNodes[2].onclick();
						continue;
					}
					i++;
				}
			}
			this.resetToolbuttonsStyle();
			this.countUncheckDeals();
			this.deleteDeals();
			localStorage.setItem(id_element.id + "", JSON.stringify(this._deals_mass))
		} else {
			alert("Выберите дела для удаления!");
		}
		return 0;
	}

	//Сброс стилей кнопок All Active Complete
	this.resetToolbuttonsStyle = function () {
		if(this.$tool_block_button_active.classList.contains("button-on")){
			this.$tool_block_button_active.classList.toggle("button-on");
			this.$tool_block_button_active.classList.toggle("button-off");
		}
		if(this.$tool_block_button_complete.classList.contains("button-on")){
			this.$tool_block_button_complete.classList.toggle("button-on");
			this.$tool_block_button_complete.classList.toggle("button-off");
		}
		if(this.$tool_block_button_all.classList.contains("button-on")){
			this.$tool_block_button_all.classList.toggle("button-on");
			this.$tool_block_button_all.classList.toggle("button-off");
		}
	}


	this.changeStyleOnButtonAll = function () {
		this.resetToolbuttonsStyle();
		this.$tool_block_button_all.classList.toggle("button-on");
		this.$tool_block_button_all.classList.toggle("button-off");
		if (this.$tool_block_button_all.classList.contains("button-off")){
			this.removeShadow();
		}
	}

	this.changeStyleOnButtonActive = function () {
		this.resetToolbuttonsStyle();
		this.$tool_block_button_active.classList.toggle("button-on");
		this.$tool_block_button_active.classList.toggle("button-off");
		if (this.$tool_block_button_active.classList.contains("button-off")){
			this.removeShadow();
		}
	}

	this.changeStyleOnButtonComplete = function () {
		this.resetToolbuttonsStyle();
		this.$tool_block_button_complete.classList.toggle("button-on");
		this.$tool_block_button_complete.classList.toggle("button-off");
		if (this.$tool_block_button_complete.classList.contains("button-off")){
			this.removeShadow();
		}
	}

	this.unloadFunction = function () {
		let i = 0;
		let loc = window.localStorage.getItem(id_element.id)
		if (loc != null){
			this._deals_mass = JSON.parse(loc);
		} else {
			return;
		}

		while(this._deals_mass[i]){
			let item_add = this._deals_mass[i];
			this.createDeal(item_add);		
			i++;
		}
		this.countUncheckDeals();
	}

	this.unloadFunction();

	
	this.$tool_block_button_all.addEventListener('click', this.selectAllDeals.bind(this));
	this.$tool_block_button_active.addEventListener('click', this.selectActiveDeals.bind(this));
	this.$tool_block_button_complete.addEventListener('click', this.selectCompleteDeals.bind(this));
	this.$tool_block_button_clear.addEventListener('click', this.clickOnButtonClearItems.bind(this));
}